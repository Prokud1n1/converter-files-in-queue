import { Express } from 'express';
import { Format } from '../../types';

export type TransformItem = {
  userToken: string;
  fileToken: string;
  filepath?: string;
  formatFrom?: string;
  formatTo: Format;
  file: Express.Multer.File;
  quality?: number;
};
