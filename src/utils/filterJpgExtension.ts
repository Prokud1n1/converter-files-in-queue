import path from 'path';
import { Express } from 'express';

export const filterJpgExtension = (file: Express.Multer.File) => {
  const fileExtension = path.extname(file.originalname);
  return fileExtension === '.jpg';
};
