import express from 'express';
import multer from 'multer';

import {
  UnsignedUploadController,
  UploadController,
} from './upload.controller';
import {
  DownloadController,
  UnauthorizedDownloadController,
} from './download.controller';
import {
  StatusController,
  UnauthorizedStatusController,
} from './status.controller';
import { MULTER_DEST } from '../constants';
import { passportValidation } from '../../auth/passport.validation';

const router = express.Router();
const upload = multer({ dest: MULTER_DEST });

router.post(
  '/upload',
  upload.array('files'),
  passportValidation(UnsignedUploadController),
  UploadController
);

router.get(
  '/download/:fileId',
  passportValidation(UnauthorizedDownloadController),
  DownloadController
);

router.get(
  '/status/:id',
  passportValidation(UnauthorizedStatusController),
  StatusController
);

export default router;
