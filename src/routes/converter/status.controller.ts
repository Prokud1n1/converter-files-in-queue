import { NextFunction, Request, Response } from 'express';
import storage from '../../store/converterStore';
import { PassportUser } from '../../types/user';

export function StatusController(req: Request, res: Response) {
  const fileId = req.params.id;
  // const userToken = req.cookies?.token;
  const userToken =
    (req.user as PassportUser)?.id || (req.headers?.token as string) || '';
  if (!userToken) {
    res.json({ err: 'could not check status ' });
  }

  const status = storage.getStatusFile({ userToken, fileToken: fileId });

  if (status) {
    return res.json({
      status: status,
    });
  } else {
    return res.json({ error: 'files not found' });
  }
}

export function UnauthorizedStatusController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  return next();
}
