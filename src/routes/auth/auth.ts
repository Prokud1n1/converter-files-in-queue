import express from 'express';
import { passport } from '../../auth/passport.setup';
import { passportValidation } from '../../auth/passport.validation';
import { privateController } from './private.controller';
import jwt from 'jsonwebtoken';
import { PassportUser } from '../../types/user';

const routes = express.Router();

routes.post('/private', passportValidation(), privateController);

routes.get(
  '/google',
  passport.authenticate('google', { scope: ['profile', 'email'] })
);
routes.get(
  '/google/callback',
  passport.authenticate('google', {
    failureRedirect: '/login',
    failureMessage: true,
  }),
  (req, res) => {
    const user = req.user as PassportUser;

    if (user?.id) {
      res.json({
        token: jwt.sign({ id: user.id }, process.env.PASSPORT_SECRET_KEY!),
      });
    } else {
      res.redirect('/');
    }
  }
);

export { routes };
