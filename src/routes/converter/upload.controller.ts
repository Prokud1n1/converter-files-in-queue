import { Express, Request, Response } from 'express';
import { nanoid } from 'nanoid';
import storage from '../../store/converterStore';
import { TransformItem } from '../../services/converter/types';
import { converterQueue } from '../../services/converter/converterQueue';
import { PassportUser } from '../../types/user';
import { filterJpgExtension } from '../../utils/filterJpgExtension';
import { Format } from '../../types';
import userStorage from '../../store/usersStore';
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';

const addFilesToQueue = (
  files: Express.Multer.File[],
  values: { userToken: string; outputFormat: Format }
) => {
  const newFiles = storage.addFiles(values.userToken, files);
  const fileIdList = Object.keys(newFiles);

  for (const [fileId, file] of Object.entries(newFiles)) {
    const task: TransformItem = {
      userToken: values.userToken,
      fileToken: fileId,
      formatTo: values.outputFormat,
      file: file.file,
    };

    converterQueue.add(task);
  }

  return fileIdList;
};

export async function UploadController(req: Request, res: Response) {
  const userToken = String((req.user as PassportUser).id);

  const files = req.files as Express.Multer.File[];

  if (files?.length) {
    const fileIdList = addFilesToQueue(files, {
      userToken,
      outputFormat: req.body.outputFormat,
    });

    return res.json({ fileIdList });
  } else {
    return res.json({ error: 'files not found' });
  }
}

export async function UnsignedUploadController(req: Request, res: Response) {
  if (req.body.outputFormat.toLowerCase() !== 'webp') {
    return res.json({ error: 'You can convert only jpg => webp format' });
  }
  const userToken = req.cookies?.token || nanoid();
  const user = userStorage.getUser(userToken);

  if (!user) {
    const subscriptionDate = dayjs.utc().format();

    userStorage.setUnauthorizedUser(userToken, {
      id: userToken,
      subscriptionDate,
    });
  }

  const files = req.files as Express.Multer.File[];

  const jpgFiles = (files ?? []).filter(filterJpgExtension);

  if (jpgFiles.length) {
    const countOfUploadedFiles = storage.getCountOfUploadedFiles(userToken);

    if (countOfUploadedFiles + jpgFiles.length > 20) {
      const availableNumberOfFiles = 20 - countOfUploadedFiles;
      return res.json({
        error: `your file upload limit is 20. You can only upload ${availableNumberOfFiles} more files`,
      });
    }

    const fileIdList = addFilesToQueue(jpgFiles, {
      userToken,
      outputFormat: req.body.outputFormat,
    });

    return res.json({ fileIdList });
  } else {
    return res.json({ error: 'you have to upload jpg files' });
  }
}
