import { NextFunction, Request, Response } from 'express';
import { passport } from './passport.setup';

export function passportValidation(
  unauthorizedCallback?: (
    req: Request,
    res: Response,
    next: NextFunction
  ) => any
) {
  return (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate('bearer', (err: any, user: any, info: any) => {
      if (err) return next(err);

      console.log(user);

      if (user) {
        req.user = user;

        return next();
      } else {
        if (unauthorizedCallback) {
          return unauthorizedCallback(req, res, next);
        } else {
          return res
            .status(401)
            .json({ status: 'error', code: 'unauthorized' });
        }
      }
    })(req, res, next);
  };
}
