class Store<T> {
  private storage: Map<string, T>;

  constructor() {
    this.storage = new Map();
  }

  get(key: string): T | undefined {
    return this.storage.get(key);
  }

  set(key: string, value: T): void {
    this.storage.set(key, value);
  }
}

export default Store;
