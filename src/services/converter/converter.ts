import sharp from 'sharp';

import { ConvertFile } from '../../types';

export const convertFile: ConvertFile = async ({ file, format }) => {
  const filePath = file.path;
  await sharp(filePath).toFormat(format).toFile(file.filename);

  return { outputFilename: file.filename, outputFormat: format };
};
