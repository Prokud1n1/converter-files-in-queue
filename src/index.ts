import './env.setup';
import express from 'express';
import morgan from 'morgan';
import { Express } from 'express';
import bodyParser from 'body-parser';
import router from './routes';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import { passport } from './auth/passport.setup';
import dayjs from 'dayjs';
const utc = require('dayjs/plugin/utc');

dayjs.extend(utc);

const app: Express = express();
const port = process.env.PORT;

const sessionMiddleware = session({
  // genid: req => uuidv4(),
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: {},
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(sessionMiddleware);
app.use(passport.initialize());
app.use(passport.session());
app.use(morgan('dev'));
app.use(router);

app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
