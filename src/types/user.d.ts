export interface PassportUser {
  id?: number | string;
  username: string;
  password: string;
  source?: string;
}

export interface UnauthroziedUser {
  subscriptionDate: string;
  id: string;
}
