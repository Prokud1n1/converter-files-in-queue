import { NextFunction, Request, Response } from 'express';
import { AuthRequest } from '../../types/passport';

export const privateController = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const authReq = req as AuthRequest;

  return res.json({
    status: 'ok',
    message:
      'Congratulations ' + authReq.user?.username + '. You have a token.',
  });
};
