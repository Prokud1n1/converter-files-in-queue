import Store from './store';
import { Format, SessionFile } from '../types';
import { nanoid } from 'nanoid';
import { Express } from 'express';
import dayjs from 'dayjs';

class ConverterStorage {
  store: Store<Record<string, SessionFile>>;

  constructor(store: Store<Record<string, SessionFile>>) {
    this.store = store;
  }

  #getFiles(userToken: string | number) {
    return this.store.get(String(userToken)) || {};
  }

  addFiles(userToken: string, files: Express.Multer.File[]) {
    const newFiles = files.reduce((acc, item) => {
      const fileId = nanoid();
      acc[fileId] = {
        file: item,
        status: 'in-progress',
        date: dayjs.utc().format(),
      };

      return acc;
    }, {} as Record<string, SessionFile>);
    const existingFiles = storage.get(userToken) || {};

    this.store.set(userToken, { ...existingFiles, ...newFiles });

    return newFiles;
  }

  setStatusToFile = (
    userToken: string,
    value: { fileToken: string; status: SessionFile['status'] }
  ) => {
    const files = this.store.get(userToken) || {};
    const updatedFile = files[value.fileToken];
    updatedFile.status = value.status;

    this.store.set(userToken, { ...files, updatedFile });
  };

  getStatusFile = (value: {
    userToken: string | number;
    fileToken: string;
  }) => {
    const files = this.#getFiles(value.userToken);
    const file = files[value.fileToken];

    if (file) {
      return file.status;
    }

    return '';
  };

  setOutputValuesToFile = (
    userToken: string,
    value: {
      fileToken: string;
      outputFilename: string;
      outputFormat: Format;
    }
  ) => {
    const files = this.store.get(userToken) || {};
    const file = files[value.fileToken];
    file.outputFilename = value.outputFilename;
    file.outputFormat = value.outputFormat;
    this.store.set(userToken, { ...files, file });
  };

  getFile = (value: { userToken: string | number; fileToken: string }) => {
    const files = this.#getFiles(value.userToken);
    const file = files[value.fileToken];

    return file;
  };

  getCountOfUploadedFiles(userToken: string | number) {
    const now = dayjs.utc();
    let count = 0;

    Object.values(this.#getFiles(userToken)).forEach(
      (sessionFile: SessionFile) => {
        const uploadDate = dayjs.utc(sessionFile.date);
        const diffInDays = now.diff(uploadDate, 'day');

        if (diffInDays <= 30) {
          count++;
        }
      }
    );

    return count;
  }
}

const storage = new Store<Record<string, SessionFile>>();

const converterStorage = new ConverterStorage(storage);

export default converterStorage;
