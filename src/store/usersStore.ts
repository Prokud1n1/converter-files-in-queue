import Store from './store';
import { PassportUser, UnauthroziedUser } from '../types/user';

class UsersStore {
  store: Store<PassportUser | UnauthroziedUser>;

  constructor(store: Store<PassportUser>) {
    this.store = store;
  }

  getUser(id: string) {
    return this.store.get(id);
  }

  setUser(id: string, user: PassportUser) {
    return this.store.set(id, user);
  }

  setUnauthorizedUser(id: string, user: UnauthroziedUser) {
    return this.store.set(id, user);
  }
}

const storage = new Store<PassportUser>();

const userStorage = new UsersStore(storage);

export default userStorage;
