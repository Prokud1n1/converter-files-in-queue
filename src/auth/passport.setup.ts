import jwt, { GetPublicKeyOrSecret, Secret } from 'jsonwebtoken';
import passport from 'passport';
import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import { Strategy as BearerStrategy } from 'passport-http-bearer';
import { PassportUser } from '../types/user';
import userStorage from '../store/usersStore';

passport.serializeUser((user, done) => {
  done(null, (user as PassportUser)?.id);
});

passport.deserializeUser(async (id, done) => {
  const user = userStorage.getUser(id as string);

  done(null, user);
});

passport.use(
  new BearerStrategy((token, cb) => {
    jwt.verify(
      token,
      process.env.PASSPORT_SECRET_KEY as Secret | GetPublicKeyOrSecret,
      (err, decoded) => {
        if (err) return cb(err);
        const decodeId = (decoded as any)?.id as string | undefined;
        let user = undefined;

        if (decodeId !== undefined) {
          user = userStorage.getUser(decodeId);
        }

        return cb(null, user ? user : false);
      }
    );
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID!,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET!,
      callbackURL: process.env.GOOGLE_CALLBACK!,
    },
    (token, tokenSecret, profile, cb) => {
      let user = userStorage.getUser(profile.id);

      if (!user) {
        user = {
          id: profile.id,
          username: profile.username || profile.displayName,
          password: profile.displayName,
          source: profile.provider,
        };
        userStorage.setUser(profile.id, user);
      }

      return cb(null, user);
    }
  )
);

export { passport };
