import { NextFunction, Request, Response } from 'express';
import storage from '../../store/converterStore';
import fs from 'fs';
import { MULTER_DEST } from '../constants';
import { PassportUser } from '../../types/user';

export function DownloadController(req: Request, res: Response) {
  const fileId = req.params.fileId;
  // const userToken = req.cookies?.token;
  const userToken =
    (req.user as PassportUser)?.id || (req.headers?.token as string) || '';

  const file = storage.getFile({ userToken, fileToken: fileId });
  const filePath = `${MULTER_DEST}${file.outputFilename}`;

  if (file) {
    const fileStream = fs.createReadStream(filePath);
    res.setHeader(
      'Content-disposition',
      `attachment; filename=${file.outputFilename}`
    );
    res.setHeader('Content-type', 'image/jpeg');
    fileStream.pipe(res);
  } else {
    res.status(500).json({ error: 'Could not download file.' });
  }
}

export function UnauthorizedDownloadController(
  req: Request,
  res: Response,
  next: NextFunction
) {
  return next();
}
