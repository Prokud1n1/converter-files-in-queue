import express from 'express';
import converterRoutes from './converter';
import { routes as loginRoutes } from '../routes/auth/auth';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('server is working!!!');
});

router.use('/converter', converterRoutes);
router.use('/auth', loginRoutes);

export default router;
